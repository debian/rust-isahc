rust-isahc (1.7.2+ds-33) unstable; urgency=medium

  * add metadata about upstream project
  * update git-buildpackage config:
    + filter out debian subdir
    + simplify usage comments
  * update watch file:
    + improve filename mangling
    + use Github API
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 07 Feb 2025 09:24:21 +0100

rust-isahc (1.7.2+ds-32) unstable; urgency=medium

  * relax patch 1001_env_logger to accept newer branch;
    bump build-dependency for crate rust-env-logger

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Jan 2025 12:00:48 +0100

rust-isahc (1.7.2+ds-31) unstable; urgency=medium

  * tighten patch 1001_event-listener
  * drop autopkgtests,
    impossible to build without private crate testserver

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Dec 2024 20:06:55 +0100

rust-isahc (1.7.2+ds-30) experimental; urgency=medium

  * stop mention dh-cargo in long description
  * add patch cherry-picked upstream
    to use newer branch of crate polling,
    superseding patch 2001_polling;
    closes: bug#1067251, thanks to Matthias Geiger and Ananthu C V

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Dec 2024 19:01:32 +0100

rust-isahc (1.7.2+ds-29) experimental; urgency=medium

  * reduce autopkgtest to check single-feature tests only on amd64
  * add patch 1001_futures-lite
    to accept newer branch of crate futures-lite;
    tighten (build-)dependencies for crate futures-lite

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 17 Aug 2024 09:00:02 +0200

rust-isahc (1.7.2+ds-28) experimental; urgency=medium

  * autopkgtest-depend on dh-rust (not dh-cargo)
  * tolerate test failures when targeted experimental

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 18 Jul 2024 18:05:06 +0200

rust-isahc (1.7.2+ds-27) experimental; urgency=medium

  [ Jonas Smedegaard ]
  * add patches cherry-picked upstream:
    + misc warning cleanups
    + support newer major version of libcurl
    reduce patch 2003
  * add patches 1001_*
    to accept newer branches of crates async-channel event-listener;
    tighten (build-)dependencies
    for crates async-channel event-listener
  * add patch 2005 to use crate os_pipe (not sluice);
    (build-)depend on package for crate os_pipe (not sluice);
    thanks to Matthias Geiger and Blair Noctis (see bug#1069621)
  * simplify packaging;
    build-depend on dh-sequence-rust
    (not dh-cargo libstring-shellquote-perl)
  * declare compliance with Debian Policy 4.7.0
  * relax to (build-/autopkgtest-)depend unversioned
    when version is satisfied in Debian stable

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 15 Jul 2024 09:43:56 +0200

rust-isahc (1.7.2+ds-26) unstable; urgency=medium

  * tighten DEP-3 patch headers
  * add patch 2001_polling to accept newer branch of crate polling;
    bump (build-)dependencies for crate polling

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 23 Feb 2024 08:54:14 +0100

rust-isahc (1.7.2+ds-25) unstable; urgency=medium

  * update patches
  * build-depend unconditionally on test-only build-dependencies
    (needed by cargo install)
  * have autopgktest skip skip all but one test,
    due to use of non-distributed crate testserver;
    closes: bug#1060848, thanks to Matthias Geiger
  * update dh-cargo fork
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 26 Jan 2024 10:48:00 +0100

rust-isahc (1.7.2+ds-24) unstable; urgency=medium

  * update DEP-3 patch headers
  * update dh-cargo fork

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 13:50:08 +0200

rust-isahc (1.7.2+ds-23) unstable; urgency=medium

  * add patch 1001_test-case
    to extend dependency to match newer system crate test-case 3.1.0;
    update build- or autopkgtest-dependencies for crate test-case

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 14 Aug 2023 00:52:15 +0200

rust-isahc (1.7.2+ds-22) unstable; urgency=medium

  * extend patch 2003 to cover failing curl version tests;
    renumber patch 2005 -> 1002;
    update DEP-3 patch headers

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 13 Aug 2023 14:36:47 +0200

rust-isahc (1.7.2+ds-21) unstable; urgency=medium

  * update dh-cargo fork
  * add patch 1001_env_logger to match system crate env_logger;
    update build- and autopkgtest-dependencies for crate env_logger
  * renumber patches 2002_* -> 1001_*
  * build-depend on package for crate publicsuffix

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 10 Aug 2023 21:12:20 +0200

rust-isahc (1.7.2+ds-20) unstable; urgency=medium

  * extend patch 2005;
    really closes: bug#1030635, thanks (again) to Adrian Bunk

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 11 Feb 2023 01:04:40 +0100

rust-isahc (1.7.2+ds-19) unstable; urgency=medium

  * add patch cherry-picked upstream
    to update tiny_http requirement from 0.9 to 0.12;
    drop now obsolete patch 2002_tiny_http
  * add patch 2005 to skip failing tests on s390x;
    closes: bug#1030635, thanks to Adrian Bunk and werdahias

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 08 Feb 2023 19:01:03 +0100

rust-isahc (1.7.2+ds-18) unstable; urgency=medium

  * fix patch 2004

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 05 Feb 2023 12:00:05 +0100

rust-isahc (1.7.2+ds-17) unstable; urgency=medium

  * add patch 2004 to use local static Public Suffix List;
    (build-)depend on publicsuffix

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Feb 2023 19:54:01 +0100

rust-isahc (1.7.2+ds-16) unstable; urgency=medium

  * add patch 2003 to avoid compiling broken tests

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 04 Feb 2023 12:59:48 +0100

rust-isahc (1.7.2+ds-15) unstable; urgency=medium

  * drop patch 2004 obsoleted by changes in crate threadfin
  * drop patch 2003, instead avoiding avoid checking all examples

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 03 Feb 2023 13:14:53 +0100

rust-isahc (1.7.2+ds-14) unstable; urgency=medium

  * tighten autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 18:29:24 +0100

rust-isahc (1.7.2+ds-13) unstable; urgency=medium

  * fix dependencies in autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 02 Feb 2023 09:05:03 +0100

rust-isahc (1.7.2+ds-12) unstable; urgency=medium

  * stop superfluously provide virtual unversioned feature packages
  * fix syntax in autopkgtests

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 01 Feb 2023 18:28:31 +0100

rust-isahc (1.7.2+ds-11) unstable; urgency=medium

  * declare compliance with Debian Policy 4.6.2
  * change binary library package to be arch-independent
  * update dh-cargo fork
  * update copyright info: update coverage
  * update patch 2002_indicatif;
    tighten build- and autopkgtest-dependency on package
    for crate indicatif
  * fix syntax error in autopkgtest-dependencies

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 27 Jan 2023 15:36:58 +0100

rust-isahc (1.7.2+ds-10) unstable; urgency=medium

  * add Bug reference to patch 2004
  * fix unconditionally build-depend
    on librust-threadfin-0.1+default-dev

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 13 Dec 2022 05:22:55 +0100

rust-isahc (1.7.2+ds-9) unstable; urgency=medium

  * update patch 2004 to check for target_has_atomic=64

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 13 Dec 2022 04:15:32 +0100

rust-isahc (1.7.2+ds-8) unstable; urgency=medium

  * add patch 2004
    to limit use of crate threadfin to supported architectures;
    limit build- and autopkgtest-dependency on package
    for crate threadfin on architectures armel mipsel s390x

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 12 Dec 2022 21:47:45 +0100

rust-isahc (1.7.2+ds-7) unstable; urgency=medium

  * fix patch 2002_tiny_http

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 12 Dec 2022 18:28:38 +0100

rust-isahc (1.7.2+ds-6) unstable; urgency=medium

  * update DEP-3 patch headers for patch 2002_castaway
  * fix update patch 2002_tiny_http to use tiny_server::ListenAddr;
    skip related still broken tests;
    tighten build- and autopkgtest-dependency
    on package for crate tiny_server
  * add patch 2003 to avoid broken example

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 12 Dec 2022 16:01:08 +0100

rust-isahc (1.7.2+ds-5) unstable; urgency=medium

  * tighten slightly patch 2002
  * check testsuite during build;
    build-depend on librust-test-case-2+default-dev
  * have autopkgtests skip check tests
    due to failure compiling testserver on armel and mipsel,
    possible caused by testserver being a private crate
  * update dh-cargo fork;
    build-depend on libstring-shellquote-perl;
    stop set X-Cargo-Crate

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 12 Dec 2022 12:22:46 +0100

rust-isahc (1.7.2+ds-4) unstable; urgency=medium

  * build-depend on librust-threadfin-0.1+default-dev
    librust-tracing-subscriber-0.3+default-dev;
    have autopkgtests depend on librust-test-case-2+default-dev
  * update local fork of dh-cargo
  * set X-Cargo-Crate to pick right crate in workspace
  * release to unstable even though still broken
    (caught by autopkgtests)
    to address (assumably accidental) package hijack

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 08 Nov 2022 08:29:33 +0100

rust-isahc (1.7.2+ds-3) experimental; urgency=medium

  * add patch 2002_parking_lot
    to relax dependency on crate parking_lot
  * tidy DEP-3 patch headers
  * fix depend on librust-parking-lot-0.12+default-dev
    (not librust-parking-lot-0.11+default-dev)
  * extend long description

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 28 Oct 2022 09:43:59 +0200

rust-isahc (1.7.2+ds-2) experimental; urgency=medium

  * avoid metapackages,
    using the equivalent of debcargo setting collapse_features=true
  * fix depend on librust-crossbeam-utils-0+default-dev
    (not librust-crossbeam-utils-0.8+default-dev)
  * update patch 2002_tiny_http patch
    to extend (not only relax downwards) dependency on crate tiny_http;
    update build-dependency on librust-tiny-http-0+default-dev
  * fix have autopkgtests depend on librust-tiny-http-0+default-dev
    (not librust-tiny-http-0.9+default-dev)

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 27 Oct 2022 23:28:26 +0200

rust-isahc (1.7.2+ds-1) experimental; urgency=low

  * initial Release;
    closes: Bug#1022811

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 26 Oct 2022 18:58:35 +0200
