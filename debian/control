Source: rust-isahc
Section: rust
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-rust,
 librust-async-channel-2+default-dev,
 librust-castaway-0.2+default-dev,
 librust-crossbeam-utils-0+default-dev (<< 0.9),
 librust-curl-0.4+default-dev,
 librust-curl-0.4+http2-dev,
 librust-curl-0.4+static-curl-dev,
 librust-curl-sys-0.4+default-dev,
 librust-curl-sys-0.4+spnego-dev,
 librust-encoding-rs-0.8+default-dev,
 librust-env-logger-0.11+default-dev,
 librust-event-listener-5+default-dev,
 librust-flate2-1+default-dev,
 librust-futures-lite-2+default-dev,
 librust-http-0.2+default-dev,
 librust-humantime-2+default-dev,
 librust-indicatif-0.17+default-dev,
 librust-log-0.4+default-dev,
 librust-mime-0.3+default-dev,
 librust-once-cell-1+default-dev,
 librust-os-pipe-1+default-dev,
 librust-polling-3+default-dev,
 librust-publicsuffix-2+std-dev,
 librust-rayon-1+default-dev,
 librust-regex-1+default-dev,
 librust-serde-json-1+default-dev,
 librust-slab-0.4+default-dev,
 librust-static-assertions-1+default-dev,
 librust-structopt-0.3+default-dev,
 librust-tempfile-3+default-dev,
 librust-test-case-3+default-dev,
 librust-threadfin-0.1+default-dev,
 librust-tiny-http-0.12+default-dev,
 librust-tracing-0.1+default-dev,
 librust-tracing-0.1+log-dev,
 librust-tracing-futures-0.2+std-dev,
 librust-tracing-futures-0.2+std-future-dev,
 librust-tracing-subscriber-0.3+default-dev,
 librust-url-2+default-dev,
 librust-waker-fn-1+default-dev,
 publicsuffix,
Maintainer: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/debian/rust-isahc.git
Vcs-Browser: https://salsa.debian.org/debian/rust-isahc
Homepage: https://github.com/sagebind/isahc
Rules-Requires-Root: no

Package: librust-isahc-dev
Architecture: all
Multi-Arch: foreign
Depends:
 librust-async-channel-2+default-dev,
 librust-castaway-0.2+default-dev,
 librust-crossbeam-utils-0+default-dev (<< 0.9),
 librust-curl-0.4+default-dev,
 librust-curl-0.4+http2-dev,
 librust-curl-0.4+static-curl-dev,
 librust-curl-0.4+static-ssl-dev,
 librust-curl-sys-0.4+default-dev,
 librust-curl-sys-0.4+spnego-dev,
 librust-encoding-rs-0.8+default-dev,
 librust-event-listener-5+default-dev,
 librust-futures-lite-2+default-dev,
 librust-http-0.2+default-dev,
 librust-httpdate-1+default-dev,
 librust-log-0.4+default-dev,
 librust-mime-0.3+default-dev,
 librust-once-cell-1+default-dev,
 librust-os-pipe-1+default-dev,
 librust-parking-lot-0.12+default-dev,
 librust-polling-3+default-dev,
 librust-publicsuffix-2+default-dev,
 librust-publicsuffix-2+std-dev,
 librust-serde-1+default-dev,
 librust-serde-json-1+default-dev,
 librust-slab-0.4+default-dev,
 librust-tracing-0.1+default-dev,
 librust-tracing-0.1+log-dev,
 librust-tracing-futures-0.2+std-dev,
 librust-tracing-futures-0.2+std-future-dev,
 librust-url-2+default-dev,
 librust-waker-fn-1+default-dev,
 publicsuffix,
 ${misc:Depends},
Provides:
 librust-isahc-1+cookies-dev (= ${binary:Version}),
 librust-isahc-1+default-dev (= ${binary:Version}),
 librust-isahc-1+http2-dev (= ${binary:Version}),
 librust-isahc-1+json-dev (= ${binary:Version}),
 librust-isahc-1+psl-dev (= ${binary:Version}),
 librust-isahc-1+spnego-dev (= ${binary:Version}),
 librust-isahc-1+static-curl-dev (= ${binary:Version}),
 librust-isahc-1+static-ssl-dev (= ${binary:Version}),
 librust-isahc-1+text-decoding-dev (= ${binary:Version}),
 librust-isahc-1+unstable-interceptors-dev (= ${binary:Version}),
 librust-isahc-1-dev (= ${binary:Version}),
 librust-isahc-1.7-dev (= ${binary:Version}),
 librust-isahc-1.7.2-dev (= ${binary:Version}),
Description: practical HTTP client that is fun to use - Rust source code
 Isahc (pronounced like Isaac) is a practical HTTP client
 that is fun to use.
 .
 Key features:
  * Full support for HTTP/1.1 and HTTP/2.
  * Configurable request timeouts, redirect policies, Unix sockets,
    and many more settings.
  * Offers an ergonomic synchronous API
    as well as a runtime-agnostic asynchronous API
    with support for async/await.
  * Fully asynchronous core,
    with incremental reading and writing of request and response bodies
    and connection multiplexing.
  * Sessions and cookie persistence.
  * Automatic request cancellation on drop.
  * Uses the http crate as an interface for requests and responses.
 .
 This package contains the source for the Rust isahc crate,
 for use with cargo.
